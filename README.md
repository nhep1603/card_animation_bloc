# card_3d_animation_bloc

- Bloc + ObserverBloc
- AnimatedBuilder + TickerProviderStateMixin
- ListView.builder
- List.generate
- Stack + Positioned
- LayoutBuilder
- GestureDetector + InkWell + Material
- Transform + Matrix4
- AbsorbPointer
- PageRouteBuilder + FadeTransition
- AnimatedWidget
- Hero + flightShuttleBuilder
- ValueChanged
- PhysicalModel
- ClipRRect

![card_3d_animation_bloc](3d-card-animations.mp4)
