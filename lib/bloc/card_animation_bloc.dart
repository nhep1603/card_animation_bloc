import 'package:bloc/bloc.dart';
import 'package:card_3d_animation_bloc/card.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

part 'card_animation_event.dart';
part 'card_animation_state.dart';

class CardAnimationBloc extends Bloc<CardAnimationEvent, CardAnimationState> {
  CardAnimationBloc({
    required this.cards,
  }) : super(CardAnimationInitial()) {
    on<CardAnimationLoaded>(_onLoad);
    on<CardAnimationInitedTicker>(_onInitedTicker);
    on<CardAnimationClicked>(_onClick);
    on<CardAnimationSelected>(_onSelect);
    on<CardAnimationReverseSelected>(_onReverseSelect);
  }
  final List<Card3D> cards;
  late AnimationController animationSelection;
  late AnimationController animationMovement;

  _onLoad(CardAnimationLoaded event, Emitter<CardAnimationState> emit) {
    if (cards.length != 0) {
      emit(CardAnimationLoadSuccess(cards: cards));
    } else {
      emit(CardAnimationLoadFailure());
    }
  }

  _onInitedTicker(
      CardAnimationInitedTicker event, Emitter<CardAnimationState> emit) {
    animationSelection = AnimationController(
      vsync: event.ticker,
      duration: const Duration(milliseconds: 500),
      lowerBound: 0.15,
      upperBound: 0.6,
    );
    animationMovement = AnimationController(
      vsync: event.ticker,
      duration: const Duration(milliseconds: 750),
    );
  }

  _onClick(CardAnimationClicked event, Emitter<CardAnimationState> emit) async {
    if (!state.selectedMode) {
      await animationSelection.forward().whenComplete(() {
        emit(CardAnimationLoadSuccess(
          cards: cards,
          selectedMode: true,
        ));
      });
    } else {
      await animationSelection.reverse().whenComplete(() {
        emit(CardAnimationLoadSuccess(
          cards: cards,
          selectedMode: false,
        ));
      });
    }
  }

  _onSelect(CardAnimationSelected event, Emitter<CardAnimationState> emit) {
    emit(CardAnimationLoadSuccess(
      cards: cards,
      selectedMode: false,
      selectedIndex: event.indexSelected,
    ));
    animationMovement.forward();
  }

  _onReverseSelect(CardAnimationReverseSelected event, Emitter<CardAnimationState> emit) {
    emit(CardAnimationLoadSuccess(
      cards: cards,
      selectedMode: true,
      selectedIndex: state.selectedIndex,
    ));
    animationMovement.reverse(from: 1.0);
  }

  @override
  Future<void> close() {
    animationSelection.dispose();
    animationMovement.dispose();
    return super.close();
  }
}
