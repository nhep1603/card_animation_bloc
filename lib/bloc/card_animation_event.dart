part of 'card_animation_bloc.dart';

abstract class CardAnimationEvent extends Equatable {
  const CardAnimationEvent();

  @override
  List<Object> get props => [];
}

class CardAnimationLoaded extends CardAnimationEvent {}

class CardAnimationInitedTicker extends CardAnimationEvent {
  const CardAnimationInitedTicker({
    required this.ticker,
  });

  final TickerProvider ticker;

  @override
  List<Object> get props => [ticker];
}

class CardAnimationClicked extends CardAnimationEvent {}

class CardAnimationSelected extends CardAnimationEvent {
  const CardAnimationSelected({
    required this.indexSelected,
  });
  final int indexSelected;

  @override
  List<Object> get props => [indexSelected];
}

class CardAnimationReverseSelected extends CardAnimationEvent {}
