part of 'card_animation_bloc.dart';

abstract class CardAnimationState extends Equatable {
  const CardAnimationState({
    this.cards = const [],
    this.selectedMode = false,
    this.selectedIndex = 0,
  });

  final List<Card3D> cards;
  final bool selectedMode;
  final int selectedIndex;

  @override
  List<Object> get props => [
        cards,
        selectedMode,
        selectedIndex,
      ];
}

class CardAnimationInitial extends CardAnimationState {}

class CardAnimationLoadSuccess extends CardAnimationState {
  const CardAnimationLoadSuccess({
    required this.cards,
    this.selectedMode = false,
    this.selectedIndex = 0,
  }) : super(cards: cards);

  final List<Card3D> cards;
  final bool selectedMode;
  final int selectedIndex;

  @override
  List<Object> get props => [
        cards,
        selectedMode,
        selectedIndex,
      ];
}

class CardAnimationLoadFailure extends CardAnimationState {}
