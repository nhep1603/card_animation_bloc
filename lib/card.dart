class Card3D {
  const Card3D({
    required this.title,
    required this.author,
    required this.image,
  });
  final String title;
  final String author;
  final String image;
}

const cardList = [
  Card3D(
    title: 'Havana',
    author: 'Camila Cabello',
    image: 'assets/images/1.jpg',
  ),
  Card3D(
    title: 'Perfect',
    author: 'Ed Sheeran',
    image: 'assets/images/2.jpg',
  ),
  Card3D(
    title: 'Human',
    author: 'One Republic',
    image: 'assets/images/3.jpg',
  ),
  Card3D(
    title: 'Star boy',
    author: 'The weeknd',
    image: 'assets/images/4.jpg',
  ),
  Card3D(
    title: 'Evolve',
    author: 'Imagine Dragons',
    image: 'assets/images/5.jpg',
  ),
];
