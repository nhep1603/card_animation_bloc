import 'dart:math';
import 'dart:ui';

import 'package:card_3d_animation_bloc/3d_card_details.dart';
import 'package:card_3d_animation_bloc/app_bloc_observer.dart';
import 'package:card_3d_animation_bloc/bloc/card_animation_bloc.dart';
import 'package:card_3d_animation_bloc/card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  Bloc.observer = AppBlocObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          CardAnimationBloc(cards: cardList)..add(CardAnimationLoaded()),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Home(),
      ),
    );
  }
}

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.black87,
        ),
        title: Text(
          'My playlist',
          style: TextStyle(
            color: Colors.black87,
          ),
        ),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.menu,
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.search,
            ),
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
            flex: 3,
            child: CardsVertical(),
          ),
          Expanded(
            flex: 1,
            child: CardsHorizontal(),
          ),
        ],
      ),
    );
  }
}

class CardsHorizontal extends StatelessWidget {
  const CardsHorizontal({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Recently Played',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 18.0,
          ),
        ),
        BlocBuilder<CardAnimationBloc, CardAnimationState>(
          builder: (context, state) {
            return state is CardAnimationLoadSuccess
                ? Expanded(
                    child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    padding: const EdgeInsets.all(10.0),
                    itemCount: state.cards.length,
                    itemBuilder: (context, index) {
                      return Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card3DWidget(card: state.cards[index]),
                      );
                    },
                  ))
                : Text('Don\'t have data');
          },
        ),
      ],
    );
  }
}

class Card3DWidget extends StatelessWidget {
  const Card3DWidget({
    Key? key,
    required this.card,
  }) : super(key: key);
  final Card3D card;

  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
      color: Colors.transparent,
      borderRadius: BorderRadius.circular(15.0),
      elevation: 10,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: Image.asset(
          card.image,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class CardsVertical extends StatefulWidget {
  const CardsVertical({Key? key}) : super(key: key);

  @override
  _CardsVerticalState createState() => _CardsVerticalState();
}

class _CardsVerticalState extends State<CardsVertical>
    with TickerProviderStateMixin {
  @override
  void initState() {
    context
        .read<CardAnimationBloc>()
        .add(CardAnimationInitedTicker(ticker: this));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CardAnimationBloc, CardAnimationState>(
      builder: (context, state) {
        return LayoutBuilder(
          builder: (context, constraints) {
            return AnimatedBuilder(
              animation: context
                  .select((CardAnimationBloc bloc) => bloc.animationSelection),
              builder: (context, child) {
                return GestureDetector(
                  onTap: () {
                    context
                        .read<CardAnimationBloc>()
                        .add(CardAnimationClicked());
                  },
                  child: Transform(
                    alignment: Alignment.center,
                    transform: Matrix4.identity()
                      ..setEntry(3, 2, 0.001)
                      ..rotateX(context.select((CardAnimationBloc bloc) =>
                          bloc.animationSelection.value)) //? tạo độ nghiêng
                    ,
                    child: state is CardAnimationLoadSuccess
                        ? AbsorbPointer(
                            absorbing: !state.selectedMode,
                            child: Container(
                              padding: EdgeInsets.all(20.0),
                              height: constraints.maxHeight,
                              width: constraints.maxWidth * 0.65,
                              color: Colors.transparent,
                              child: Stack(
                                clipBehavior: Clip.none,
                                children: [
                                  ...List.generate(
                                    4,
                                    (index) => Card3DItem(
                                      card: state.cards[index],
                                      animatedSelectionValue: context.select(
                                          (CardAnimationBloc bloc) =>
                                              bloc.animationSelection.value),
                                      index: index,
                                      height: constraints.maxWidth / 2,
                                      onCardSelected: (value) {
                                        _onCardSelected(
                                            state.cards[index], index);
                                      },
                                      verticalFactor:
                                          _getCurrentFactor(index, context),
                                      animatedMovementValue: context.select(
                                          (CardAnimationBloc bloc) =>
                                              bloc.animationMovement),
                                    ),
                                  ).reversed
                                ],
                              ),
                            ),
                          )
                        : Text('Don\'t have data'),
                  ),
                );
              },
            );
          },
        );
      },
    );
  }

  _onCardSelected(Card3D card, int index) {
    context
        .read<CardAnimationBloc>()
        .add(CardAnimationSelected(indexSelected: index));
    Navigator.of(context).push(
      PageRouteBuilder(
        transitionDuration: const Duration(milliseconds: 750),
        pageBuilder: (context, animation, secondaryAnimation) {
          return FadeTransition(
            opacity: animation,
            child: CardsDetails(card: card),
          );
        },
      ),
    );
  }

  _getCurrentFactor(int index, BuildContext context) {
    final _selectedIndex =
        context.select((CardAnimationBloc bloc) => bloc.state.selectedIndex);
    if (index == _selectedIndex) {
      return 0;
    } else if (index > _selectedIndex) {
      return -1;
    } else {
      return 1;
    }
  }
}

class Card3DItem extends AnimatedWidget {
  const Card3DItem({
    Key? key,
    required this.card,
    required this.animatedSelectionValue,
    required this.index,
    required this.height,
    required this.onCardSelected,
    this.verticalFactor = 0,
    required Animation<double> animatedMovementValue,
  }) : super(
          key: key,
          listenable: animatedMovementValue,
        );

  final Card3D card;
  final double animatedSelectionValue;
  final int index;
  final double height;
  final ValueChanged<Card3D> onCardSelected; //? tương tự VoidCallback
  final int verticalFactor;
  Animation<double> get animatedMovementValue =>
      listenable as Animation<double>;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 0,
      left: 0,
      top: height + height / 2 * animatedSelectionValue * -index, //? xếp lớp
      child: Transform(
        alignment: Alignment.center,
        transform: Matrix4.identity()
          ..setEntry(3, 2, 0.001)
          ..translate(
            0.0,
            verticalFactor *
                animatedMovementValue.value *
                MediaQuery.of(context).size.height, //? chiều dọc (trục y)
            index * 50.0, //? chiều sâu (trục z)
          ),
        child: Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () => onCardSelected(card),
            child: Hero(
              tag: card.title,
              //? xoay nhiều vòng hero
                flightShuttleBuilder: (flightContext, animation,
                    flightDirection, fromHeroContext, toHeroContext) {
                  Widget _current;
                  if (flightDirection == HeroFlightDirection.push) {
                    _current = toHeroContext.widget;
                  } else {
                    _current = fromHeroContext.widget;
                  }
                  return AnimatedBuilder(
                    animation: animation,
                    builder: (context, child) {
                      final newValue = lerpDouble(0.0, 2 * pi, animation.value);
                      // print('animation : ${animation.value}');
                      // print('newValue : $newValue');
                      return Transform(
                        alignment: Alignment.center,
                        transform: Matrix4.identity()
                          ..setEntry(3, 2, 0.001)
                          ..rotateX(newValue!),
                        child: _current,
                      );
                    },
                  );
                },
              child: Card3DWidget(
                card: card,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
