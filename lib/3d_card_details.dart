import 'package:card_3d_animation_bloc/bloc/card_animation_bloc.dart';
import 'package:card_3d_animation_bloc/card.dart';
import 'package:card_3d_animation_bloc/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CardsDetails extends StatelessWidget {
  const CardsDetails({
    Key? key,
    required this.card,
  }) : super(key: key);
  final Card3D card;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
        iconTheme: IconThemeData(
          color: Colors.black45,
        ),
        leading: BackButton(onPressed: () {
          Navigator.of(context).pop();
          context.read<CardAnimationBloc>().add(CardAnimationReverseSelected());
        },),
      ),
      body: Column(
        children: [
          SizedBox(
            height: size.height * 0.1,
          ),
          Align(
            child: SizedBox(
              height: 150.0,
              child: Hero(
                tag: card.title,
                child: Card3DWidget(card: card),
              ),
            ),
          ),
          const SizedBox(
            height: 20.0,
          ),
          Text(
            card.title,
            style: TextStyle(
              color: Colors.black54,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 20.0,
          ),
          Text(
            card.author,
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.w600,
            ),
          )
        ],
      ),
    );
  }
}
